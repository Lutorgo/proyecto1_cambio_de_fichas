# Proyecto1_Cambio_de_fichas


# Proyecto1_Cambio_de_fichas

  

# Proyecto

. Valor: 20%.

Curso: Programación II.

Docente: Alejandro Alfaro Quesada. (aalfaroq@utn.ac.cr)

Fecha de entrega: domingo 05 de marzo del 2023.

Hora de entrega: 05:00 pm.

## Indicaciones Generales.

  • La prueba se desarrolla bajo la modalidad de proyecto, con tiempo
   de inicio y fin predeterminado.

• Toda prueba evaluada es considerada como un único proyecto, es decir, aunque la evaluación sea dividida por secciones, la completitud de uno o más rubros puede afectar la funcionalidad del sistema, y esto, podría influir en la nota final.

• La prueba puede ser desarrollada en un máximo de dos personas. Para los que están conformados en parejas, se debe contemplar la siguiente regla:

 Si por alguna razón no puedan continuar trabajando juntos, ambos deben continuar con el código fuente que tienen hasta ese momento; es decir, no se permite que se unan a otro compañero y/o utilizar el código fuente de otro compañero.

• Si algún estudiante es sorprendido en actos fraudulentos, ya sea que los provoque o los consienta, su prueba y la de los demás implicados, será automáticamente anulada.

• El docente ejecuta el código fuente que está en el proyecto enviado para comprobar que realiza lo solicitado. Por lo que, el docente NO modificará ninguna línea de código, por lo tanto, debe asegurarse que el código fuente funciona correctamente.

• El proyecto con el código solo se recibe y se valida desde el Campus Virtual de la UTN. Por cualquier otro medio que se envíe al docente no será revisado.

• En el proyecto debe utilizar programación por capas, objetos, archivos, estructuras de datos e interfaz gráfica obligatoriamente.

  

## Resolución del juego

  

• El tablero debe estar representado en la interfaz tal y como se muestra en la 
imagen de ejemplo.

• El sistema no puede limitarse a una única manera de resolver el juego, si no,  que, mediante las reglas del juego, el usuario debe analizar los movimientos.

• El juego tiene las siguientes reglas que se deben respetar en programación:

 - Las piezas se pueden mover solamente hacia adelante.
 - Se puede hacer uno o más movimientos a la vez, de cada color de pieza
   de manera adyacente.
 - Se puede saltar una única vez cada pieza, siempre y cuando sea de
   diferente color.

  

## Archivos

  

• Cada vez, que el juego se reinicie o el usuario logre encontrar la solución, se 
debe guardar la siguiente información en un único archivo.

 - Nombre del usuario.
 - Fecha y hora (cuando se registró para iniciar el juego).
 - Cantidad de movimientos adyacentes.
 - Cantidad de saltos.
 - Duración total de tiempo del juego.
 - Un campo que sea para el tipo de juego ya sea que lo reinició o que
   encontró la solución.

• Algunos ejemplos de la información que se guarda en el archivo:

 - Alejandro | 12/02/2021 6:00 PM | 33 | 12 | 08:02 | Reinicio
   
   Alejandro | 12/02/2021 6:32 PM | 21 | 12 | 04:00 | Reinicio
   
   Alejandro | 14/02/2021 7:32 PM | 40 | 18 | 10:12 | Resuelto

## Reportes

• Se deben crear los siguientes reportes utilizando un JTable para mostrar la información:

1. Menor cantidad de movimientos adyacentes hecho por un único usuario. Debe cargar los nombres de los participantes en un combo y, al elegir uno de ellos, mostrar el nombre, fecha, hora y la cantidad de movimientos.

2. Cantidad de veces que se resolvió el juego por cada participante. Debe mostrar el nombre de cada participante y la cantidad de veces que lo resolvió. Tome en cuenta que es un único registro que se muestra, independientemente el usuario lo haya resuelto muchas veces.

3. Cantidad de veces que un único usuario reinició el juego. Se hace la búsqueda por una persona en específico a través de un combo. Debe mostrar el nombre y la cantidad de reinicios. Tome en cuenta que es un único registro que se muestra, independientemente el usuario lo haya reiniciado muchas veces.
